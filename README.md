About this Dockerized Django project starter:

This Django project starter was created following the guide at https://docs.docker.com/compose/django/ by using Docker Compose to set up and run a Django/PostgreSQL app. 
Before starting, need to install Docker, Docker Compose, Python 3.X as well as pip in your system.


For installation of docker, refer to:
1. https://nickjanetakis.com/blog/should-you-use-the-docker-toolbox-or-docker-for-mac-windows
2. https://docs.docker.com/docker-for-mac/install/
3. https://docs.docker.com/toolbox/toolbox_install_mac/
4. https://docs.docker.com/docker-for-windows/install/
5. https://docs.docker.com/toolbox/toolbox_install_windows/
6. https://docs.docker.com/engine/installation/linux/ubuntu/

	check: 	docker --version
			docker-compose --version
			Python --version, python3 --version, python3.6 --version
			pip --version
	For  command list: docker --help, 


If Docker was installed with Docker toolbox, always run docker related command in Docker Quickstart terminal.


I. Define the project components in project directory:
	
	1. Dockerfile:

		 FROM python:3
		 ENV PYTHONUNBUFFERED 1
		 RUN mkdir /code
		 WORKDIR /code
		 ADD requirements.txt /code/
		 RUN pip install -r requirements.txt
		 ADD . /code/

	2. requirements.txt:

		 Django>=1.8,<2.0
		 psycopg2

	3. docker-compose.yml:

		version: '3'

		services:
		  db:
		    image: postgres
		  web:
		    build: .
		    command: python3 manage.py runserver 0.0.0.0:8000
		    volumes:
		      - .:/code
		    ports:
		      - "8000:8000"
		    depends_on:
		      - db


II. Create a Django project in the root of project directory:
	(This will build and create project image and container)

	> docker-compose run web django-admin.py startproject djangoStarter .


	if on Linux, change the ownership of the new files:

	> sudo chown -R $USER:$USER .


III. Connect the database:
	 Edit the djangoStarter/settings.py file with following:

	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.postgresql',
	        'NAME': 'postgres',
	        'USER': 'postgres',
	        'HOST': 'db',
	        'PORT': 5432,
	    }
	}



IV. Run the container:

	1. Run the docker-compose up command from the top level directory:

		> docker-compose up

		or: if want to rebuild container/image

		> docker-compose up --build


	2.visit the site in browser:
		> docker-machine ip 

		go to: http://<Docker-Host-IP>:8000 
			or  http://localhost:8000  (DOCKER for MAC or WIN)
				http://192.168.99.100:8000 (Docker toolbox)


		Note: On Windows 10, you need to edit ALLOWED_HOSTS inside settings.py and add your Docker host name 
		or IP address to the list. This value is not safe for production usage. 

			ALLOWED_HOSTS = ['192.168.99.100']

	3. develop app in the dockerized VM


V. stop and restart the docker container:

	1. List running containers or images, and get ID# or Name:

		> docker ps
		> docker-compose ps
		> docker images

	2. Enter container via bash:
		> docker exec -it containerName bash/ash
		e.g.
		> docker exec -it djangostarterdocker_web_1 bash
		...
		> exit

	3. Shut down, clean up services or remove container/image:

		> Ctrl-C  
		> docker-compose down
		> docker-compose stop

		# remove indivdual container or image
		> docker rm -f containerID#
		> docker rmi -f imageName/ID#

		# remove stopped containers:
		> docker-compose rm -f

		# Remove "dangling" images
		> docker rmi -f $(docker images -qf dangling=true)

	4. Re-build or restart container:

		> docker-compose up --build
		> docker-compose up